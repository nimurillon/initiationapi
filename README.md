# Initiation API Rest

## Installation de fastAPI :

Dans un terminal, tapper la commande suivante :

```pip install fastAPI[all]```

Ceci devrait installer fastAPI ainsi que d'autres dépendances qui permettront de faire tourner votre serveur en local.

Placez-vous ensuite dans le dossier contenant le projet (avec le fichier `main.py`) et tapper la commande :

```uvicorn main:app --reload```

Cette commande va lancer votre serveur. Voilà ce qu'elle fait en détail :

-  main: Le fichier `main.py` (Le module Python).
- app: Se réfère à l'objet créé dans le fichier `main.py` avec la ligne `app = FastAPI()`.
- --reload: Restart le serveur à chaque changement de code.

**Il se peut que le path ne se mette pas à jour automatiquement**, dans ce cas il vous faudra ajouter le chemib vers le dossier contenant l'exécutable `uvicorn`. Sur Turing, la commande suivante devrait régler le problème.

```export PATH="$HOME/.local/bin:$PATH"```

## Création de la base de données

Pour créer la base de données, il vous suffit de lancer la commande (toujours en étant dans le dossier contenant le projet) :

```python3 load_db.py```

La base de données va ainsi être créées avec quelques données à l'intérieur.

## Strucuture du projet

Plusieurs dossiers sont fournis :
- config : contient un fichier permettant de se connecter à la base de données
- Controller : dossier qui contiendra les différents contrôleurs de notre applications
- db : dossier contenant la base de données
- DTO : Dossier contenant les Data Transfer Object représentant nos objets en base de données
- Entity : Dossier contenant la définition de nos différents Objets (ou entité)
- Repository : Dossier contenant les différents repository lié à chaque entité

## Définir une entité

Dans un premier temps, on peut définir une entité Hotel (création d'un fichier hotel.py dans le dossier Entity). Pour cela, nous allons devoir importer plusieurs chose :

```python
from sqlalchemy.sql.schema import Column
from sqlalchemy.sql.sqltypes import Integer, String
from config.bdd import Base
```

**Column** va nous permettre d'indiquer que l'on souhaite ajouter un attribut à notre entité.
**Integer** et **String** permettent d'indiquer le type de l'attribut ajouté.
Enfin, **Base** est une classe dont notre entité va hériter. Ceci permet d'indiquer que notre entié représente un objet en base.

Il suffit ensuite de déclarer notre classe avec les attributs comme suit :

```python
class Hotel(Base):
    __tablename__ = "hotels"
    id = Column(Integer, primary_key=True, index=True) # On définit l'id comme la clé primaire
    # TODO : définir les autres attributs
```

**RQ** : ***\_\_tablename\_\_*** représente le nom de la table dans lequel nos objets seront stockés.

## Définir un DTO

De même que pour une entité, nous allons à présent créer un fichier `hotelDTO.py` dans le dossier `DTO`. Nous allons alors créer une classe **HotelDTO** qui héritera de la classe `BaseModel`. On peut ensuite définir les attributs de cette classe comme suit :

```python
from pydantic import BaseModel

class HotelDTO(BaseModel):
    id: int
    # TODO : déclarer les autres attributs
```

## Définir un repository 

Un repository est une classe, qui permet de faire des requêtes sur la base de données concernant une entité. Dans notre cas, on pourra faire un repository pour les hôtels, et faire des recherches d'hôtels par nom, par id, par ville...

On créé donc un fichier `hotelRepository.py` dans le dossier `Repository`, puis on créé une classe **HotelRepository** contenant les méthodes permettant de récupérer les données de la base de données. Pour cela, on passe en paramètre de nos méthodes un objet de type **Session**, représentant la base de données sur laquelle effectuer la requête

Appellons **db** notre objet de type **Session**, la méthode **query** permet de précisier l'entité que l'on recherche (ici il s'agit d'un hôtel, donc on lui passera la classe **Entity.hotel.Hotel** en paramètre). On appelle ensuite la fonction **all()** pour récupérer l'ensemble des objets retournés par la requête :

```python
from sqlalchemy.orm.session import Session
import Entity.hotel

class HotelRepository():
    def get_hotels(self, db: Session):
        return db.query(Entity.hotel.Hotel).all()
```

Si on veut faire une recherche par id d'un hôtel, alors on peut ajouter un *filtre* à notre requête. Il suffit pour cela d'appeler la méthode **filter** juste après la méthode **query** et de préciser que l'on cherche un hôtel avec un id précis :

```python
def get_hotel_by_id(self, db: Session, id: int):
        return db.query(Entity.hotel.Hotel).filter(Entity.hotel.Hotel.id == id).first()
```

## The last but not the least : Le contrôleur

Le contrôleur, c'est ce qui va nous permettre de déclarer les différents points d'entrée de notre API. C'est lui qui va *écouter* les requêtes entrantes sur les différentes routes que nous auront définies et de renvoyer les résultats.

Dans un premier temps, il faut créer le fichier `hotelController` dans le dossier `Controller`, puis il faut importer plusieurs choses :
- **APIRouteur** : c'est ce qui va nous permettre de définir nos routes (ou points d'accès de l'API)
- **Depends** : permets de passer la bonne base de données en paramètre d'une route
- **Session** : permet de manipuler la base de données
- **get_db** : méthode retournant la base données que l'on manipule
- **HotelRepository** : le repository créé dans la partie précédente

```python
from fastapi import APIRouter
from fastapi.params import Depends
from sqlalchemy.orm.session import Session
from config.bdd import get_db
from Repository.hotelRepository import HotelRepository
```

On va ensuite définir un **router**, c'est-à-dire l'objet qui va nous permettre de créer nos différents routes :

```python
router = APIRouter(
    prefix="/hotels",
    tags=["hotels"]
)
```

Ici, **prefix="/hotels"** permet d'indiquer que toutes les routes que nous allons définir grâce à ce router commenceront par /hotels. Par exemple, si l'adresse de votre serveur est **http://localhost:8000**, alors tous les url permettant de faire des requêtes sur les hotels commencerons par **http://localhost:8000/hotels**. **tags=["hotels"]** est utile pour la génération d'une documentation (non utilisée ici)

On crée ensuite une instance de la classe **HotelRepository** :
```python
hotelRepo = HotelRepository()
```

Il ne reste plus qu'à définir notre première route :

```python
@router.get("/")
async def getHotels(db: Session = Depends(get_db)):
    return hotelRepo.get_hotels(db)
```

On précise ici que le router doit écouter les requêtes **get** sur la route **/hotels/**. Cette méthode prend en paramètre une session (pour aller chercher nos données) et retourne le résultat de l'appel à la méthode **get_hotels** de notre HotelRepository. Le résultat est ensuite retourné au client au format JSON.

On peut également utiliser `@router.post` et `@router.put` pour écouter les requêtes POST et PUT (pour faire un CRUD complet)

## Dernière étape : Intégration des routeurs dans l'application

On vient de définir notre premier routeur, mais on ne précise pas comment il doit être intégré dans l'application. Commençons par reprendre le contenu de notre fichier `main.py` : 

```python
from fastapi import FastAPI

app = FastAPI()
```

Ici on déclare notre application **app**, mais pour que l'on puisse accéder au différentes routes que l'on vient de créer dans notre contrôleur, il faut préciser à app que l'on va utiliser ce contrôler. Pour cela il suffit de deux étapes :


1. Importer notre contrôleur : `from Controller import hotelController`
2. Ajouter le contrôleur à notre application : `app.include_router(hotelController.router)`

Ce qui donne :

```python
from fastapi import FastAPI
from Controller import hotelController

app = FastAPI()
app.include_router(hotelController.router)
```

Et voilà, votre API est prête, il ne vous reste plus qu'à tester tout ça !

## Tester l'API

Pour vérifier que tout fonctionne bien, nous allons utiliser **Swagger**, qui est déjà intégré dans notre applications. Pour cela, rendez-vous sur **http://localhost:8000/docs**, vous aurez alors différentes parties qui vous permettront de tester vos différentes routes (voir démo en live)