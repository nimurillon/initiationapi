from sqlalchemy.orm.session import Session
import Entity.hotel
import DTO.hotelDTO

class HotelRepository():
    def get_hotels(self, db: Session):
        return db.query(Entity.hotel.Hotel).all()

    def get_hotel_by_id(self, db: Session, id: int):
        return db.query(Entity.hotel.Hotel).filter(Entity.hotel.Hotel.id == id).first()

    def get_hotel_by_city(self, db: Session, city: str):
        return db.query(Entity.hotel.Hotel).filter(Entity.hotel.Hotel.ville.startswith(city)).all()

    def add_hotel(self, db: Session, hotel: DTO.hotelDTO.HotelDTO):
        db_hotel = Entity.hotel.Hotel(**hotel.dict())
        db.add(db_hotel)
        db.commit()
        db.refresh(db_hotel)

        return db_hotel