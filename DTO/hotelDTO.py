from pydantic import BaseModel, BaseConfig
import Entity.hotel

BaseConfig.arbitrary_types_allowed = True

class HotelDTO(BaseModel):
    name: str
    description: str
    ville = str
    adresse = str
    codePostal = int

    def toEntity(self):
        return Entity.hotel.Hotel(**self.dict())
        
    class Config:
        orm_mode = True