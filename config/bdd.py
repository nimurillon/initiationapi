from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

import os

path_to_project = os.path.realpath(os.path.dirname(os.path.dirname(__file__)))
DB_LINK = f"sqlite:///{path_to_project}/db/database.db"

engine = create_engine(DB_LINK, connect_args={"check_same_thread": False})
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
Base = declarative_base()

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()