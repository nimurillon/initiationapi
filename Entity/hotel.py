from sqlalchemy.sql.schema import Column
from sqlalchemy.sql.sqltypes import Integer, String
from config.bdd import Base
import DTO.hotelDTO

class Hotel(Base):
    __tablename__ = "hotels"
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    description = Column(String)
    ville = Column(String)
    adresse = Column(String)
    codePostal = Column(Integer)
    
    def toDTO(self):
        return DTO.hotelDTO.HotelDTO(self.__dict__)