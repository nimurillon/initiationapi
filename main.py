from fastapi import FastAPI
from Controller import hotelController

app = FastAPI()
app.include_router(hotelController.router)