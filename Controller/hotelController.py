from fastapi import APIRouter
from fastapi.params import Depends
from sqlalchemy.orm.session import Session
from config.bdd import get_db
from Repository.hotelRepository import HotelRepository
from Entity.hotel import Hotel
from DTO.hotelDTO import HotelDTO

router = APIRouter(
    prefix="/hotels",
    tags=["hotels"]
)

hotelRepo = HotelRepository()

@router.get("/")
async def getHotels(db: Session = Depends(get_db)):
    return hotelRepo.get_hotels(db)

@router.get("/id/{id}")
async def getHotelById(id: int, db: Session=Depends(get_db)):
    return hotelRepo.get_hotel_by_id(db, id)

@router.get('/city/{city}')
async def getHotelsByCity(city: str, db: Session=Depends(get_db)):
    return hotelRepo.get_hotel_by_city(db, city)

@router.post("/hotel", response_model=HotelDTO)
async def postHotel(hotel: HotelDTO, db: Session = Depends(get_db)):
    db_hotel = hotelRepo.add_hotel(db, hotel)
    return db_hotel